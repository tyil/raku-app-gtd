#! /usr/bin/env perl6

use v6.d;

use App::GTD;
use Test;

my $input = q:to/EOF/;
x (A) 2016-05-20 2016-04-30 measure space for @context +chapelShelving @chapel due:2016-05-30
hello world
EOF

plan 1;

my @records = gtd-parse-todo($input);

is @records.elems, $input.lines.elems, 'All lines parsed to records';

# vim: ft=perl6 noet
