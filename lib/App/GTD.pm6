#! /usr/bin/env false

use v6.d;

use Config;
use Grammar::TodoTxt;
use IO::Path::XDG;
use Terminal::ANSIColor;

use App::GTD::GrammarActions;

unit module App::GTD;

subset PrioStr of Str is export where /^ <[a..z A..Z]> $/;

my Config $config .= new.read: {
	colors => {
		enabled => True,
		contexts => 'green',
		projects => 'red',
		leftover-labels => 'cyan',
		labels => {
			gtd => 'yellow',
		},
	},
	data => {
		file => xdg-data-home.add('todo.txt').absolute,
	},
};

sub gtd-config-file (
	--> IO::Path
) is export {
	xdg-config-home.add('gtd/config.toml')
}

sub gtd-config-load () is export
{
	my $file = gtd-config-file;

	return unless $file.e;

	$config.=read($file.absolute);

	return;
}

multi sub gtd-config (
	--> Config
) is export {
	$config
}

multi sub gtd-config (
	Str:D $key,
	--> Any
) is export {
	$config.get($key)
}

sub gtd-parse-todo (
	Str:D $input,
	--> List
) is export {
	Grammar::TodoTxt.parse($input, actions => App::GTD::GrammarActions).made
}

sub gtd-records-read (
) is export {
	gtd-parse-todo(gtd-config('data.file').IO.slurp)
}

sub gtd-records-write (
	@records,
) is export {
	gtd-config('data.file').IO.spurt(@records.join("\n") ~ "\n")
}

sub gtd-records-string (
	@records,
	--> Str
) is export {
	my $index-length = @records.sort(*.id).tail.id.Str.chars;

	@records
		.map({
			"[%{$index-length}d] %s".sprintf(
				$_.id,
				colorize-output(~$_)
			)
		})
		.join("\n")
}

sub colorize-output (
	Str:D $input,
	Bool:D :$stderr = False,
) is export {
	return $input unless $config<colors><enabled>;

	$input
		.words
		.map({
			given $_ {
				when $_.starts-with('+') { colored($_, gtd-config('colors.projects')) }
				when $_.starts-with('@') { colored($_, gtd-config('colors.contexts')) }
				when $_.contains(':') {
					my $key = $_.split(':').first;

					colored(
						$_,
						gtd-config("colors.labels.$key")
							// gtd-config('colors.leftover-labels'),
					)
				}
				default { $_ }
			}
		})
		.join(' ')
		;
}

=begin pod

=NAME    App::GTD
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
