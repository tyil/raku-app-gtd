#! /usr/bin/env false

use v6.d;

unit class App::GTD::Record;

has Int $.id;
has Bool $.complete is rw = False;
has Str $.priority is rw = '';
has Date $.completion-date is rw;
has Date $.creation-date is rw;
has Str $.description is rw;
has %.labels;

method contexts (
	--> Iterable
) {
	$.description
		.words
		.grep(*.starts-with('+'))
}

method projects (
	--> Iterable
) {
	$.description
		.words
		.grep(*.starts-with('@'))
}

method Str (
	--> Str
) {
	my $s;

	if ($.complete) {
		$s ~= 'x ';
	}

	if ($.priority) {
		$s ~= "($.priority) ";
	}

	if ($.completion-date) {
		$s ~= $.completion-date.yyyy-mm-dd ~ ' ';
	}

	if ($.creation-date) {
		$s ~= $.creation-date.yyyy-mm-dd ~ ' ';
	}

	$s ~= $!description;

	if (%.labels) {
		$s ~= @.labels
			.sort
			.map({
				" {$_.key}:{$_.value}"
			})
			.join('')
	}

	$s;
}

=begin pod

=NAME    App::GTD::Record
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
