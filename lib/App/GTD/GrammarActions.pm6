#! /usr/bin/env false

use v6.d;

use App::GTD::Record;

unit class App::GTD::GrammarActions;

my $line-number = 1;

method TOP ($/)
{
	make $<records>
		.map({
			my $record = $_.made<record>;
			my %labels = $record<description><labels>.map(*.made);
			my %data =
				id => $_.made<line-number>,
				complete => ?$record<completion-marker>,
				labels => %labels,
				description => $record<description>
					.Str
					.words
					.grep(* ∉ $record<description><labels>.map(*.Str))
					.join(' ')
					,
			;

			if ($record<priority>) {
				%data<priority> = ~$_<priority>;
			}

			if ($record<completion>) -> $date {
				%data<completion-date> = Date.new(~$date);
			}

			if ($record<creation>) -> $date {
				%data<creation-date> = Date.new(~$date);
			}

			App::GTD::Record.new(|%data)
		})
		.List
}

method record ($/)
{
	make %(
		line-number => $line-number++,
		record => $/,
	)
}

method label ($/)
{
	make ~$<key> => ~$<value>
}

=begin pod

=NAME    App::GTD::GrammarActions
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
