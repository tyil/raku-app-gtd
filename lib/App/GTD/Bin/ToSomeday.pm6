#! /usr/bin/env false

use v6.d;

use App::GTD;

unit module App::GTD::Bin::ToSomeday;

#| Move an item to your next items.
multi sub MAIN (
	'someday',

	#| The ID of the Thing to operate on.
	Int:D $id!,

	#| Update the description, if given.
	*@words,
) is export {
	gtd-config-load;

	my @records = gtd-records-read;

	@records[+$id - 1].labels<gtd> = 'someday';
	@records[+$id - 1].description = @words.join(' ') if @words;

	gtd-records-write(@records);
}

=begin pod

=NAME    App::GTD::Bin::Someday
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
