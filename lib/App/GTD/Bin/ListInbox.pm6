#! /usr/bin/env false

use v6.d;

use App::GTD;

unit module App::GTD::Bin::ListInbox;

#| List items in your inbox.
multi sub MAIN (
	'inbox',
) is export {
	gtd-config-load;

	my @records = gtd-records-read()
		.grep({
			$_.labels<gtd>:exists
			&& $_.labels<gtd> eq 'inbox'
			&& !$_.complete
		})
		.sort({ .creation-date, .description })
		;

	if (!@records) {
		say 'Your inbox is empty!';
		return 0;
	}

	gtd-records-string(@records).say;
}

=begin pod

=NAME    App::GTD::Bin::ListInbox
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
