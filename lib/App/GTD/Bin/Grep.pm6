#! /usr/bin/env false

use v6.d;

use App::GTD;

unit module App::GTD::Bin::Grep;

#| Search through your items.
multi sub MAIN (
	'grep',
	*@query where .words > 0,
) is export {
	gtd-config-load;

	my @records = gtd-records-read.grep({ description-filter($_, @query) });

	return 0 unless @records;

	gtd-records-string(@records).say;
}

sub description-filter (
	$record,
	@filters,
	--> Bool
) {
	my @words = $record.description.words;

	for @filters {
		return False unless $_ ∈ @words;
	}

	return True;
}

=begin pod

=NAME    App::GTD::Bin::Grep
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
