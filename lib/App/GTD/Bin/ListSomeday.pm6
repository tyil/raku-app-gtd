#! /usr/bin/env false

use v6.d;

use App::GTD;

unit module App::GTD::Bin::ListSomeday;

#| List your next items.
multi sub MAIN (
	'someday',
) is export {
	gtd-config-load;

	my @records = gtd-records-read()
		.grep({
			$_.labels<gtd>:exists
			&& $_.labels<gtd> eq 'someday'
			&& !$_.complete
		})
		.sort({ .creation-date, .description })
		;

	if (!@records) {
		say 'You have no items for someday';
		return 0;
	}

	gtd-records-string(@records).say;
}

=begin pod

=NAME    App::GTD::Bin::ListSomeday
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
