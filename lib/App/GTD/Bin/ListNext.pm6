#! /usr/bin/env false

use v6.d;

use App::GTD;

unit module App::GTD::Bin::ListNext;

#| List your next items.
multi sub MAIN (
	'next',
) is export {
	gtd-config-load;

	my @records = gtd-records-read()
		.grep({
			$_.labels<gtd>:exists
			&& $_.labels<gtd> eq 'next'
			&& !$_.complete
		})
		.sort({ !.priority, .creation-date, .description })
		;

	if (!@records) {
		say 'You have no next items!';
		return 0;
	}

	gtd-records-string(@records).say;
}

=begin pod

=NAME    App::GTD::Bin::ListNext
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
