#! /usr/bin/env false

use v6.d;

use App::GTD;

unit module App::GTD::Bin::ToNext;

#| Move an item to your next items.
multi sub MAIN (
	'next',

	#| The ID of the Thing to operate on.
	Int:D $id!,

	#| Set the priority of the item. When passed as --prio (without value),
	#| removes existing priority. If given a value, it *must* be a single
	#| letter.
	:$prio where * ~~ Any|Bool|PrioStr,

	#| Update the description, if given.
	*@words,
) is export {
	gtd-config-load;

	my @records = gtd-records-read;

	@records[+$id - 1].labels<gtd> = 'next';
	@records[+$id - 1].description = @words.join(' ') if @words;

	given ($prio) {
		when Bool    { @records[+$id - 1].priority = ''       }
		when PrioStr { @records[+$id - 1].priority = $prio.uc }
	}

	gtd-records-write(@records);
}

=begin pod

=NAME    App::GTD::Bin::InboxToNext
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
