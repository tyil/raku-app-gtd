#! /usr/bin/env false

use v6.d;

use App::GTD;

unit module App::GTD::Bin::Add;

#| Add a new item to your inbox.
multi sub MAIN (
	'add',
	*@words where .words > 0,
) is export {
	gtd-config-load;

	# Create a record out of the arguments given
	my $record = @words.join(' ').&gtd-parse-todo.first;

	# Alter the record to represent a new item in the GTD inbox
	$record.creation-date = now.Date;
	$record.labels<gtd> = 'inbox';

	# Append the record to todo.txt
	gtd-config('data.file').IO.spurt(~$record ~ "\n", :append);
}

=begin pod

=NAME    App::GTD::Bin::Add
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
