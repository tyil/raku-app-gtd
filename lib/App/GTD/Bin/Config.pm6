#! /usr/bin/env false

use v6.d;

use Data::Dump;
use JSON::Fast;

use App::GTD;

unit module App::GTD::Bin::Config;

#| Show the current configuration. This is mainly useful for debugging
#| purposes.
multi sub MAIN (
	'config',
) is export {
	gtd-config-load;

	say(Dump(gtd-config(), :skip-methods));
}

#| Shows the current configuration as a JSON blob.
multi sub MAIN (
	'config',
	Bool :$json! where { $_ },
) is export {
	gtd-config-load;

	say(to-json(gtd-config.get, :sorted-keys))
}

=begin pod

=NAME    App::GTD::Bin::Config
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
