#! /usr/bin/env false

use v6.d;

use App::GTD;

unit module App::GTD::Bin::Done;

#| Mark an item as done.
multi sub MAIN (
	'done',

	#| The ID of the item to operate on.
	Int:D $id!,

	*@ids,
) is export {
	gtd-config-load;

	my @records = gtd-records-read;

	@records[+$id - 1].complete = True;
	@records[+$id - 1].completion-date = now.Date;

	gtd-records-write(@records);
}

=begin pod

=NAME    App::GTD::Bin::Done
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
