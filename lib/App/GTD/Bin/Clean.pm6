#! /usr/bin/env false

use v6.d;

use App::GTD;

unit module App::GTD::Bin::Clean;

#| Clean up your GTD file, removing any entries marked as done.
multi sub MAIN (
	'clean',
) is export {
	gtd-config-load;

	my @records = gtd-records-read()
		.grep(!*.complete)
		.sort({ !.priority, .creation-date, .description })
		;

	gtd-records-write(@records);
}

=begin pod

=NAME    App::GTD::Bin::Clean
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
